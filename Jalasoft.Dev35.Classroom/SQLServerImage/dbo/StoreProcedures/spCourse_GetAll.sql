﻿CREATE PROCEDURE [dbo].[spCourse_GetAll]
AS
BEGIN
	SELECT Id, CourseName, Description, StartingDate, EndingDate, Image, CreatedAt, UpdatedAt 
	FROM dbo.[Course]
	WHERE State = 1;
END