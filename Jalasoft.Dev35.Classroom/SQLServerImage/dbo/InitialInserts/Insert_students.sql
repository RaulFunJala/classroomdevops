INSERT INTO 
	Student (FirstName, LastName, Email, Birthday, Image, CountryId, CourseId)
VALUES
	('Martin', 'Guillen', 'martin.guillen@fundacion-jala.com', '2000-10-10', 'http://localhost:8080/api/Images/student/2022721615_MartinGuillen.png', 1, null),
	('Mauro', 'Cordova', 'mauro.cordova@fundacion-jala.com', '2001-10-10', 'http://localhost:8080/api/Images/student/2022721615_MauroCordova.png', 2, 1),
	('Nelson', 'Rodriguez', 'nelson.rodriguez@fundacion-jala.com', '2002-10-10', 'http://localhost:8080/api/Images/student/2022721615_NelsonRodriguez.png', 3, 1),
	('Andres', 'Endara', 'andres.endara@fundacion-jala.com', '2002-10-10', 'http://localhost:8080/api/Images/student/2022721615_AndresEndara.png', 4, null),
	('Jimena', 'Miranda', 'jimena.miranda@fundacion-jala.com', '1999-10-10', 'http://localhost:8080/api/Images/student/2022721615_JimenaMiranda.png', 5, 1),
	('Patricia', 'Alvarez', 'patricia.alvarez@fundacion-jala.com', '2000-10-10', 'http://localhost:8080/api/Images/student/2022721616_PatriciaAlvarez.png', 6, 1),
	('Andrea', 'Vasquez', 'andrea,vasquez@fundacion-jala.com', '1998-10-10', 'http://localhost:8080/api/Images/student/2022721616_AndreaVasquez.png', 7, null),
	('Jose', 'Vilaseca', 'jose.vilaseca@fundacion-jala.com', '1996-10-10', 'http://localhost:8080/api/Images/student/2022721616_JoseVilaseca.png', 8, 1),
	('Mariana', 'Martinez', 'mariana.martinez@fundacion-jala.com', '2000-10-10', 'http://localhost:8080/api/Images/student/2022721616_MarianaMartinez.png', 3, 1),
	('Maria', 'Arancibia', 'maria.arancibia@fundacion-jala.com', '2001-10-10', 'http://localhost:8080/api/Images/student/2022721617_MariaArancibia.png', 5, null),
	('Eduardo', 'Villa', 'eduardo.villa@fundacion-jala.com', '2020-10-10', 'http://localhost:8080/api/Images/student/2022721617_EduardoVilla.png', 8, 1),
	('Nicole', 'Velez', 'nicole.velez@fundacion-jala.com', '2000-10-10', 'http://localhost:8080/api/Images/student/2022721617_NicoleVelez.png', 2, 2),
	('Saara', 'Solares', 'saara.solares@fundacion-jala.com', '2001-10-10', 'http://localhost:8080/api/Images/student/2022721617_SaaraSolares.png', 1, null),
	('Daniel', 'Ban', 'daniel.ban@fundacion-jala.com', '2001-10-10', 'http://localhost:8080/api/Images/student/2022721618_DanielBan.png', 7, 2),
	('Alejandro', 'Castro', 'alejandro.castro@fundacion-jala.com', '2000-10-10', 'http://localhost:8080/api/Images/student/2022721618_AlejandroCatro.png', 4, 2),
	('Karina', 'Lopez', 'karina.lopez@fundacion-jala.com', '1998-10-10', 'http://localhost:8080/api/Images/student/2022721618_KarinaLopez.png', 3, null),
	('Kathia', 'Fernandez', 'kathia.fernandez@fundacion-jala.com', '1998-10-10', 'http://localhost:8080/api/Images/student/2022721618_KathiaFernandez.png', 5, 2),
	('Patricia', 'Perez', 'patricia.perez@fundacion-jala.com', '2000-10-10', 'http://localhost:8080/api/Images/student/2022721619_PatriciaPerez.png', 7, 2),
	('Eduardo', 'Erguez', 'eduardo.erguez@fundacion-jala.com', '1999-10-10', 'http://localhost:8080/api/Images/student/2022721619_EduardoErguez.png', 6, null),
	('Antonio', 'Tobar', 'antonio.tobar@fundacion-jala.com', '1999-10-10', 'http://localhost:8080/api/Images/student/2022721619_AntonioTobar.png', 2, 3),
	('Melisa', 'Urdininea', 'melisa.urdininea@fundacion-jala.com', '2000-10-10', 'http://localhost:8080/api/Images/student/2022721619_MelisaUrdininea.png', 5, 3);