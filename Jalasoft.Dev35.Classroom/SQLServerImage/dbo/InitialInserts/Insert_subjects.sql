INSERT INTO 
	[Subject] (SubjectName, StartingDate, TrainerName, StartTime, EndTime, DaysOfWeek, Image, CourseId)
VALUES
	('Frontend', '2020-10-20', 'Amilkar Contreras', '17:00', '18:00', '21', 'http://localhost:8080/api/Images/subject/1-Frontend.png', 1),
	('Backend', '2020-10-21', 'Raúl Gamarra', '16:30', '18:00', '10', 'http://localhost:8080/api/Images/subject/1-Backend.png', 1),
	('DevOps', '2020-10-20', 'Alejandro Sanchez', '9:00', '10:00', '21', 'http://localhost:8080/api/Images/subject/1-Devops.png', 1),
	('Monitoring', '2020-10-23', 'José Ecos', '17:00', '18:00', '31', 'http://localhost:8080/api/Images/subject/1-Monitoring.png', 1),
	('English', '2020-10-21', 'Gabriela Gutierrez', '14:30', '15:10', '31', 'http://localhost:8080/api/Images/subject/1-English.png', 1),
    ('Frontend', '2021-10-20', 'Richard Ecos', '17:00', '18:00', '17', 'http://localhost:8080/api/Images/subject/2-Frontend.png', 2),
	('Backend', '2021-10-21', 'Josué Lima', '16:30', '18:00', '21', 'http://localhost:8080/api/Images/subject/2-Backend.png', 2),
	('DevOps', '2021-10-20', 'Alejandro Sanchez', '9:00', '10:00', '21', 'http://localhost:8080/api/Images/subject/2-Devops.png', 2),
    ('Monitoring', '2021-10-23', 'José Ecos', '17:00', '18:00', '31', 'http://localhost:8080/api/Images/subject/2-Monitoring.png', 2),
	('English', '2021-10-21', 'Marcelo Zambrana', '14:30', '15:10', '31', 'http://localhost:8080/api/Images/subject/2-English.png', 2),
    ('Frontend', '2022-10-20', 'Bill Gates', '17:00', '18:00', '21', 'http://localhost:8080/api/Images/subject/3-Frontend.png', 3),
	('Backend', '2022-10-21', 'Steve Jobs', '16:30', '18:00', '10', 'http://localhost:8080/api/Images/subject/3-Backend.png', 3),
	('DevOps', '2022-10-20', 'Marc Zuckerberg', '9:00', '10:00', '21', 'http://localhost:8080/api/Images/subject/3-Devops.png', 3),
	('Monitoring', '2022-10-23', 'Sergey Brin', '17:00', '18:00', '31', 'http://localhost:8080/api/Images/subject/3-Monitoring.png', 3),
    ('Frontend', '2020-10-20', 'Marcelo Flores', '17:00', '18:00', '21', 'http://localhost:8080/api/Images/subject/4-Frontend.png', 4),
	('Backend', '2020-10-21', 'Fabiola Martinez', '16:30', '18:00', '10', 'http://localhost:8080/api/Images/subject/4-Backend.png', 4),
	('DevOps', '2020-10-20', 'Gonzalo Del Barrio', '9:00', '10:00', '21', 'http://localhost:8080/api/Images/subject/4-Devops.png', 4),
	('Monitoring', '2020-10-23', 'Manuel De La Riva', '17:00', '18:00', '31', 'http://localhost:8080/api/Images/subject/4-Monitoring.png', 4);
