CREATE PROCEDURE [dbo].[spSubject_GetAll]
@courseId INT
AS
BEGIN
	SELECT [Id], [SubjectName], [StartingDate], [TrainerName], [StartTime], [EndTime], [DaysOfWeek], [Image], [CourseId]
	FROM dbo.[Subject]
	WHERE [CourseId] = @courseId AND [State] = 1;
END