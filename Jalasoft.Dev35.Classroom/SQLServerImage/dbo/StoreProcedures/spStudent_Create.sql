﻿CREATE PROCEDURE [dbo].[spStudent_Create]
@FirstName VARCHAR(25),
@LastName VARCHAR(25),
@Email VARCHAR(50),
@Birthday DATE,
@Image VARCHAR(125),
@CountryId INT
AS
BEGIN
	INSERT INTO [dbo].[Student] 
		([FirstName], [LastName], [Email], [Birthday], [Image], [CountryId])
	VALUES
		(@FirstName, @LastName, @Email, @Birthday, @Image, @CountryId);

	SELECT TOP 1 
		student.[Id],
		student.[FirstName], 
		student.[LastName], 
		student.[Email], 
		student.[Birthday], 
		student.[Image], 
		student.[CourseId],
		student.[CountryId]
	FROM
		dbo.Student AS student 
    ORDER BY 
		student.[Id] DESC
END