export STATUS=1
i=0
while [[ $STATUS -ne 0 && $i -lt 60 ]]
do
    let i=i+1
    echo "**********************************"
    echo "Waiting for SQL Server to start..."
    /opt/mssql-tools/bin/sqlcmd -t 1 -S localhost -U sa -P Password@1234 -Q "SELECT 1;" > /dev/null
    STATUS=$?
    sleep 1 
done
if [ $STATUS -ne 0 ] 
then
    echo "Error: MSSQL SERVER took more than 60 seconds to start up."
    exit 1 
fi

echo "======= MSSQL SERVER STARTED ======"

path="/var/opt/mssql/data/CLASSROOMDB.mdf"

if [ ! -f "$path" ]
then
    echo "***************** Restoring database: ....."
    ./restore.sh
else
    echo "********* Skipping database restore. *********"
fi
    echo "======== MSSQL CONFIG COMPLETE =========="