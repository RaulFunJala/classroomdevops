﻿CREATE PROCEDURE [dbo].[spSubject_Get]
@id INT
AS
BEGIN
	SELECT [Id], [SubjectName], [StartingDate], [TrainerName], [StartTime], [EndTime], [DaysOfWeek], [Image], [CourseId]
	FROM dbo.[Subject]
	WHERE @id = [Id] AND [State] = 1;
END