﻿CREATE PROCEDURE [dbo].[spCourse_Create]
@CourseName VARCHAR(25),
@Description varchar(125),
@StartingDate DATE,
@EndingDate DATE,
@Image VARCHAR(125) = NULL
AS
BEGIN
	INSERT dbo.[Course] (CourseName, Description, StartingDate, EndingDate, Image)
	VALUES (@CourseName, @Description, @StartingDate, @EndingDate, @Image);
	SELECT TOP 1 * FROM dbo.[Course] ORDER BY Id DESC;
END