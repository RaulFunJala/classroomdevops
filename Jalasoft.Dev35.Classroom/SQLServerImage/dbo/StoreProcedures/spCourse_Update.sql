﻿CREATE PROCEDURE [dbo].[spCourse_Update]
@Id INT,
@CourseName VARCHAR(25) = NULL,
@Description varchar(125) = NULL,
@StartingDate DATE = NULL,
@EndingDate DATE = NULL,
@Image VARCHAR(125) = NULL
AS
BEGIN
	UPDATE dbo.[Course]
		SET
			[CourseName] = ISNULL(@CourseName, [CourseName]),
			[Description] = ISNULL(@Description, [Description]),
			[StartingDate] = ISNULL(@StartingDate, [StartingDate]),
			[EndingDate] = ISNULL(@EndingDate, [EndingDate]),
			[Image] = ISNULL(@Image, [Image]),
			[UpdatedAt] = GETDATE()
		WHERE Id = @Id;
	SELECT Id, CourseName, Description, StartingDate, EndingDate, Image, CreatedAt, UpdatedAt 
		FROM dbo.[Course]
		WHERE Id = @Id AND State = 1;
END
