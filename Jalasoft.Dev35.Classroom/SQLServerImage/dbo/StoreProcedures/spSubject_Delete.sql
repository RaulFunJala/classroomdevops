﻿CREATE PROCEDURE [dbo].[spSubject_Delete]
@id INT
AS
BEGIN
    UPDATE [dbo].[Subject]
    SET
        [state] = 0,
        [UpdatedAt] = GETDATE()
    WHERE @id = [Id]
END