USE CLASSROOMDB
GO

CREATE LOGIN Project
WITH PASSWORD = 'Password@1234';

CREATE USER [Project]
FOR LOGIN [Project]
WITH DEFAULT_SCHEMA=dbo;

ALTER ROLE db_owner ADD MEMBER [Project];
ALTER ROLE db_datareader ADD MEMBER [Project];
ALTER ROLE db_datawriter ADD MEMBER [Project];

ALTER SERVER ROLE sysadmin ADD MEMBER [Project];
ALTER SERVER ROLE  dbcreator  ADD MEMBER [Project];
ALTER SERVER ROLE  serveradmin  ADD MEMBER [Project];
GO