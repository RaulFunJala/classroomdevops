﻿CREATE PROCEDURE [dbo].[spStudent_Get]
@Id INT
AS
BEGIN
	SELECT 
		student.[Id],
		student.[FirstName], 
		student.[LastName], 
		student.[Email], 
		student.[Birthday], 
		student.[Image], 
		student.[State],
		student.[CourseId],
		student.[CountryId]
	FROM
		dbo.Student AS student
	WHERE
		student.[Id] = @Id
END