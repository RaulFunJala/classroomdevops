﻿CREATE PROCEDURE [dbo].[spStudent_GetAll]
AS
BEGIN
	SELECT 
		student.[Id],
		student.[FirstName], 
		student.[LastName], 
		student.[Email], 
		student.[Birthday], 
		student.[Image],
		student.[CourseId],
		student.[CountryId]
	FROM
		dbo.Student AS student
END