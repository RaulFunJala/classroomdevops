INSERT INTO 
	Course(CourseName, Description, StartingDate, EndingDate, Image)
VALUES
	('Dev34 Level3', 'In this course students must have advanced knowlegde working with Java stack', '2020-10-10', '2021-10-11', 'http://localhost:8080/api/Images/course/Dev34 Level3.png'),
	('Dev35 Level1', 'This course is focus to teach from elemental to advanced concept using .Net stack', '2021-10-10', '2022-10-11', 'http://localhost:8080/api/Images/course/Dev35 Level1.png'),
	('Dev35 Level2', 'This course is oriented to students that already have elemental concepts on NodeJs stack', '2022-07-10', '2023-03-31', 'http://localhost:8080/api/Images/course/Dev35 Level2.png'),
	('Dev36 Level1', 'Python for web development is the main approach on this course', '2020-10-10', '2020-10-11', 'http://localhost:8080/api/Images/course/Dev36 Level1.png')