﻿CREATE PROCEDURE [dbo].[spStudent_Update]
@Id INT,
@CourseId INT = NULL
AS
BEGIN
	UPDATE 
		dbo.Student
	SET
		[CourseId] = @CourseId
	FROM
		dbo.Student
	WHERE
		Id = @Id;
	EXEC spStudent_Get @id;
END