# ClassroomDevops

## Members 

* Carlos Zurita
* Jose Arcani
* Raul Camacho

## Reference to BE and FE projects

* [Web API](https://gitlab.com/RaulFunJala/ClassroomAPI)
* [Web App](https://gitlab.com/jose-arcani-jalafund/ClassroomWeb)


## Run docker compose

```
$ git clone https://gitlab.com/RaulFunJala/classroomdevops.git
$ cd classroomdevops
$ docker-compose up
```