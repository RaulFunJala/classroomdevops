CREATE PROCEDURE [dbo].[spSubject_Create]
@subjectName VARCHAR(25),
@startingDate DATE,
@trainerName VARCHAR(50),
@startTime VARCHAR(5),
@endTime VARCHAR(5),
@daysOfWeek INT,
@image VARCHAR(125),
@courseId INT = NULL
AS
BEGIN
    DECLARE @subjectId INT
    INSERT INTO [dbo].[Subject]
    (
     [SubjectName], [StartingDate], [TrainerName], [StartTime], [EndTime], [DaysOfWeek], [Image], [CourseId]
    )
    VALUES
    (
     @subjectName, @startingDate, @trainerName, @startTime, @endTime, @daysOfWeek, @image, @courseId
    )
     SET @subjectId = SCOPE_IDENTITY()
    EXEC [dbo].[spSubject_Get] @subjectId
END