﻿CREATE PROCEDURE [dbo].[spCourse_Get]
@Id INT
AS
BEGIN
	SELECT Id, CourseName, Description, StartingDate, EndingDate, Image, CreatedAt, UpdatedAt 
	FROM dbo.[Course]
	WHERE Id = @Id AND State = 1;
END