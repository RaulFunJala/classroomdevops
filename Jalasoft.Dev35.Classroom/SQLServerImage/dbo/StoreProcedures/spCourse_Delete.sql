﻿CREATE PROCEDURE [dbo].[spCourse_Delete]
@Id INT
AS
BEGIN
	UPDATE dbo.[Course]
		SET
			[State] = 0
		WHERE Id = @Id;
END

